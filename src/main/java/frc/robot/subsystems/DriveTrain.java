/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.VictorSP;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants.DriveTrainConstants;

public class DriveTrain extends SubsystemBase {
  /**
   * Creates a new DriveTrain.
   */

  private final VictorSP frontLeft = new VictorSP(DriveTrainConstants.frontLeft);
  private final VictorSP frontRight = new VictorSP(DriveTrainConstants.frontRight);
  private final VictorSP backLeft = new VictorSP(DriveTrainConstants.backLeft);
  private final VictorSP backRight = new VictorSP(DriveTrainConstants.backRight);

  private final SpeedControllerGroup leftGroup = new SpeedControllerGroup(frontLeft, backLeft);
  private final SpeedControllerGroup rightGroup = new SpeedControllerGroup(frontRight, backRight);

  private final DifferentialDrive driveTrain = new DifferentialDrive(leftGroup, rightGroup);

  private boolean isReverse = false;

  public DriveTrain() {

  }

  @Override
  public void periodic() {
    SmartDashboard.putBoolean("Is Drivetrain Reverse", isReverse);
  }

  public void curvatureDrive(double fwd, double rot, boolean quickTurn){
    driveTrain.curvatureDrive(fwd * (isReverse ? -1 : 1), rot, quickTurn);
  }

  public void toggleIsReverse(){
    isReverse = !isReverse;
  }
}
